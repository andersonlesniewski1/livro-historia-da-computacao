# Calculadoras Mecânicas

Em 1623 o alemão Wilhelm Schickard foi detentor da primeira tentativa de mecanizar o cálculo,  porém o projeto acabou sendo abandonado no ano seguinte. Uma segunda tentativa surgiu duas décadas depois nas mãos do genial Blaise Pascal, o projeto tinha o objetivo de ajudar seu pai que trabalhava coletando impostos e a calculadora cooperou na vasta quantidade de operações aritméticas. O aparelho foi chamado de Calculadora de Pascal ou Pascaline.


![](http://1.bp.blogspot.com/-KGfdZ4ztRGA/VVXTwiK24cI/AAAAAAAACbE/JFXKC4IY9Ik/s1600/10-lec1-doc1-pascaline_copy.jpg)

Avançando um pouco no tempo, mais especificamente em 1851 tivemos a chegada do Aritmômetro, uma calculadora mecânica que foi um sucesso comercial da época, o aparelho era robusto e tinha a capacidade de resolver multiplicações  e divisões por meio de um acumulador móvel, devido a suas funcionalidades o Aritmômetro passou a ser usado diariamente em várias áreas comerciais, até 1890 ele foi o único a ser produzido comercialmente.


![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Arithmometer_-_One_of_the_first_machines_with_unique_serial_number.jpg/330px-Arithmometer_-_One_of_the_first_machines_with_unique_serial_number.jpg)

Já em 1887 começaram a surgir as primeiras calculadoras mecânicas a utilizar um teclado, nesse ano tivemos o comptômetro que foi pioneira, utilizando nove tecladas para representar os dígitos.


![](https://upload.wikimedia.org/wikipedia/commons/3/3e/Comptometer_model_ST_Super_Totalizer.png)

Com o avanço da tecnologia as calculadoras mecânicas começaram a perder força, isso aconteceu devido a chegada das calculadoras eletrônicas, em 1961 tivemos um último suspiro com o Anita mk7 que mesclou os dois mundos, sendo ela a primeira calculadora mecânica com um mecanismo totalmente eletrônico. Na década de 1970 tivemos o fim da produção das calculadoras mecânicas, uma era que perdurou por volta de 120 anos.


![](https://lh3.googleusercontent.com/proxy/sL13uwDyCjH5TxUxVnMchm93AtqpiDvaoYOuUVOhIaoXgCAZcfuwYM0U16zPy7Z87VZw1i4RHo9DzSA_kcsmstj1mtMrmN_npmjzrb2_RFg7uwyaJOM)

