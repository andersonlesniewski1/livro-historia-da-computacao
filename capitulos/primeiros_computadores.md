# Os Primeiros Computadores

Considerado por muitos como o primeiro computador do mundo, o Z1 foi desenvolvido pelo inventor alemão Konrad Kuse (1910-1995) entre os anos de 1936 e 1938. O Z1 funcionava usando um sistema binário, ao contrário de seus predecessores que usavam, em sua maioria, cartões perfurados. Basicamente, o movimento de um pequeno bastão ou prato metálico, representava o número um, e o não movimento representava o zero. Nos anos seguintes, o inventor aprimorou sua obra criando o Z2, Z3 e Z4, além do S1 e S2. 

![](https://upload.wikimedia.org/wikipedia/commons/e/e5/Zuse_Z1-2.jpg)


Em 1943, motivado por questões bélicas, o governo dos Estados Unidos iniciou o Project PX, que tinha como objetivo desenvolver uma máquina que agilizasse os cálculos necessários na guerra. O projeto foi finalizado em 1946, dando origem ao primeiro computador norte-americano, chamado de Eletronic Numerical Integrator and Computer (ENIAC), computador este que pesava em torno de 30 toneladas e tinha 25 metros de comprimento por 5,50 metros de altura. Sua programação era feita através de chaves manuais, onde eram inseridos cartões perfurados com as instruções, e o processamento era feito pelas válvulas, que queimavam constantemente e exigiam frequentes manutenções.

![](https://segredosdomundo.r7.com/wp-content/uploads/2020/08/eniac-historia-e-funcionamento-do-primeiro-computador-do-mundo-2.jpg)


Já no âmbito de computadores pessoais, é importante citar o Altair 8800, projetado em 1975 e vendido como um kit básico de computação pela revista norte americana Popular Electronics. Foi através do contato com este computador que Bill Gates e Paul Allen criaram o interpretador Altair Basic, primeiro produto da Microsoft, começando uma onda de popularização dos computadores.

![](https://tecnoblog.net/meiobit/wp-content/uploads/2021/01/1280px-Altair_8800_Smithsonian_Museum.jpg)


Além da Microsoft, outra empresa foi importante no alavancamento dos computadores pessoais, uma delas sendo a Apple. Fundada por Steve Jobs e Steven Wozniak, a Apple foi responsável por tornar a interação computador-usuário mais amigável, aumentando assim o público alvo de vendas de computadores. Isto foi realizado através da produção do Apple II, um dos primeiros computadores com gráficos coloridos. Ele era equipado com um processador MOS Technology 6502 a 1 MHz, saída de vídeo que permitia exibição de gráficos de até 16 cores, placa de som, interface de disquetes. 

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Apple_II-IMG_7064.jpg/800px-Apple_II-IMG_7064.jpg)


# Referências
- https://www.researchgate.net/profile/Roberto-Lopes-2/publication/292411425_Nos_primordios_da_informatica_estudo_sobre_a_construcao_dos_primeiros_computadores_eletronicos_digitais_nos_Estados_Unidos_e_Uniao_Sovietica/links/56ae7e4e08aeaa696f2ecdac/Nos-primordios-da-informatica-estudo-sobre-a-construcao-dos-primeiros-computadores-eletronicos-digitais-nos-Estados-Unidos-e-Uniao-Sovietica.pdf
- http://fap.if.usp.br/~crepaldi/archive/ECF_Final.pdf
- https://arxiv.org/abs/1406.1886
