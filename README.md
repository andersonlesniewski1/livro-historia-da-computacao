# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores). 

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/primeiros_computadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração]()
1. [Computação Móvel]()
1. [Futuro]()




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168547/avatar.png?width=400)  | Anderson Lesniewski | andersonlesniewski.1 | [andersonlesniewski@alunos.utfpr.edu.br](mailto:andersonlesniewski@utfpr.alunos.edu.br)|
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168522/avatar.png?width=400)  | Ryan Gmieski Ferraz | ryangmieskiferraz | [ryanferraz@alunos.edu.br](mailto:ryanferraz@alunos.edu.br)|
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168499/avatar.png?width=400) | William de Morais Chakur | Willchakur | [williamchakur@alunos.utfpr.edu.br](mailto:williamchakur@alunos.utfpr.edu.br)|
